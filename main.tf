data "template_file" "userdata" {
  template = "${file("cloud-config.yaml")}"
}

variable "availability_zone" { default = "us-east-2c" }
