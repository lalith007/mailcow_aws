resource "aws_instance" "EmailServer_mailcow" {
  ami 				= "ami-7891cb1d"
  availability_zone 		= "us-east-2c"
  instance_type 		= "t2.small"
  vpc_security_group_ids 	= ["${aws_security_group.EmailServer_SG.id}", "${aws_security_group.SSH_SG.id}"]
  user_data            		= "${data.template_file.userdata.rendered}"


  root_block_device {
    volume_size = 8
    volume_type = "standard"
    delete_on_termination = false
  }

  ebs_block_device {
    device_name = "/dev/sdg"
    volume_size = 15
    volume_type = "standard"
    delete_on_termination = false
  }

  tags {
	Backup = "False"
  }

}
