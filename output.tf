
output "Email SecurityGroup Id" {
    value = "${aws_security_group.EmailServer_SG.id}"
}

output "SSH SecurityGroup Id" {
    value = "${aws_security_group.SSH_SG.id}"
}

output "EC2 Instance Id" {
    value = "${aws_instance.EmailServer_mailcow.id}"
}

output "EC2 Instance Name" {
    value = "${aws_instance.EmailServer_mailcow.name}"
}

output "EC2 Instance Private IP" {
    value = "${aws_instance.EmailServer_mailcow.private_ip}"
}

output "EC2 Instance Public IP" {
    value = "${aws_instance.EmailServer_mailcow.public_ip}"
}

